var color0
var color1
var color2
var color3
var color4
var color5
var color6
var color7
var color8
var color9
var colorA
var colorB
var colorC
var colorD
var colorE
var colorF

var colors

var bar

function setup() {
    createCanvas(640, 480);


    /* C64-RGB-PAL.vpl
       VICE Palette file
       # Syntax:
       # Red Green Blue Diter
    */
    /*
    color0 = color(0x00, 0x00, 0x00, 0x0)
    color1 = color(0xFF, 0xFF, 0xFF, 0xE)
    color2 = color(0xAB, 0x31, 0x26, 0x4)
    color3 = color(0x66, 0xDA, 0xFF, 0xC)
    color4 = color(0xBB, 0x3F, 0xB8, 0x8)
    color5 = color(0x55, 0xCE, 0x58, 0x4)
    color6 = color(0x1D, 0x0E, 0x97, 0x4)
    color7 = color(0xEA, 0xF5, 0x7C, 0xC)
    color8 = color(0xB9, 0x74, 0x18, 0x4)
    color9 = color(0x78, 0x53, 0x00, 0x4)
    colorA = color(0xDD, 0x93, 0x87, 0x8)
    colorB = color(0x5B, 0x5B, 0x5B, 0x4)
    colorC = color(0x8B, 0x8B, 0x8B, 0x8)
    colorD = color(0xB0, 0xF4, 0xAC, 0x8)
    colorE = color(0xAA, 0x9D, 0xEF, 0x8)
    colorF = color(0xB8, 0xB8, 0xB8, 0xC)
    */
    /* alpha swapped
    color0 = color(0x00, 0x00, 0x00, 0xF)
    color1 = color(0xFF, 0xFF, 0xFF, 0x2)
    color2 = color(0xAB, 0x31, 0x26, 0xC)
    color3 = color(0x66, 0xDA, 0xFF, 0x4)
    color4 = color(0xBB, 0x3F, 0xB8, 0x8)
    color5 = color(0x55, 0xCE, 0x58, 0xC)
    color6 = color(0x1D, 0x0E, 0x97, 0xC)
    color7 = color(0xEA, 0xF5, 0x7C, 0x4)
    color8 = color(0xB9, 0x74, 0x18, 0xC)
    color9 = color(0x78, 0x53, 0x00, 0xC)
    colorA = color(0xDD, 0x93, 0x87, 0x8)
    colorB = color(0x5B, 0x5B, 0x5B, 0xC)
    colorC = color(0x8B, 0x8B, 0x8B, 0x8)
    colorD = color(0xB0, 0xF4, 0xAC, 0x8)
    colorE = color(0xAA, 0x9D, 0xEF, 0x8)
    colorF = color(0xB8, 0xB8, 0xB8, 0x4)
    */

    // view-source:http://www.krajzewicz.de/blog/stretchingC64palettes/c64colors.png
    color0 = color(0, 0, 0)
    color1 = color(255, 255, 255)
    color2 = color(0x89, 0x40, 0x36)
    color3 = color(0x7a, 0xbf, 0xc7)
    color4 = color(0x8a, 0x46, 0xae)
    color5 = color(0x68, 0xa9, 0x41)
    color6 = color(0x3e, 0x31, 0xa2)
    color7 = color(0xd0, 0xdc, 0x71)
    color8 = color(0x90, 0x5f, 0x25)
    color9 = color(0x5c, 0x47, 00)
    colorA = color(0xbb, 0x77, 0x6d) // lifted from gradient
    colorB = color(0x55, 0x55, 0x55)
    colorC = color(0x80, 0x80, 0x80)
    colorD = color(0xac, 0xea, 0x88)
    colorE = color(0x7c, 0x70, 0xda)
    colorF = color(0xab, 0xab, 0xab)

    /*
    color0 = color(0, 0, 0)
    color1 = color(255, 255, 255)
    color2 = color(136, 0, 0)
    color3 = color(170, 255, 238)
    color4 = color(204, 68, 204)
    color5 = color(0, 204, 85)
    color6 = color(0, 0, 170)
    color7 = color(238, 238, 119)
    color8 = color(221, 136, 85)
    color9 = color(102, 68, 0)
    colorA = color(255, 119, 119)
    colorB = color(51, 51, 51)
    colorC = color(119, 119, 119)
    colorD = color(170, 255, 102)
    colorE = color(0, 136, 255)
    colorF = color(187, 187, 187)
*/

    colors = [color0, color1, color2, color3, color4, color5, color6, color7, color8, color9, colorA, colorB, colorC, colorD, colorE, colorF]

    bar = [

	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

	0x00,
	0x0b,
	0x0f,
	0x07,
	0x01,
	0x07,
	0x0f,
	0x0b,
	0x00,

	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,


	0, 11, 5, 12, 13, 15, 1, 15, 13, 12, 5, 11, 0,

		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,

	6, 11, 4, 14, 3, 13, 1, 13, 3, 14, 4, 11, 6,

	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    
	9, 2, 8, 10, 15, 7, 1, 7, 15, 10, 8, 2, 9,


	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0,

	]
}


function draw() {
    //    background(0)
    lines()
}  

var printed

function lines() {

    var y = 100
    var i;
    for (i = 0; i < bar.length; i++) {
	var b = bar[i]
	var c = colors[b]

	if (!printed) {
	    window.print(b + " -> " + c)
	}
	
	stroke(c)
	line(0, y, 640, y++)
	line(0, y, 640, y++)
//	line(0, y, 640, y++)
//	line(0, y, 640, y++)
    }
    printed = true

//    var colorE = color(0, 136, 255)

    
    fill(colorD)

    rect(10, 10, 20, 20)
}

function xofm(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)
    
    return width/2 - 200*s;
}

function yofm(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)

    var f4 = cos((f+50)*inc2)
    var f5 = cos((f+150)*inc3)


    return height/2 + 100*s*f5 - 100*fs*f5*f4 - 20*fs*f5;
}

function xof(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)

    var f4 = cos((f+50)*inc2)
    var f5 = cos((f+150)*inc3)

    return width/2 + 100*s*f4 - 100*f4*f5 + 40*fs*s;
}

function yof(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)
    
    return height/2 + 200*s + 50*fs;
}


function letter(l, offset, xfunc, yfunc) {
    var fr = frameCount + offset*10
    
    var fs = cos(fr*incf)

    var s = -sin(fr*incf)
    
    var x = xfunc(fr)
    var y = yfunc(fr)
//    var x = width/2 + 200*s;
    // var y = height/2 + 200*s + 50*fs;

//    textSize(40+20*fs)
    strokeWeight(3)
    
    var col = abs(x/2-320)+160
    
    col = 255;
    stroke(0, 255, 0)
    fill(col, col, col)

    text(l, x, y)
//    rect(x, y, 3, 3)
}





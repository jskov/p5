var incf;
var inc2;
var inc3

function setup() {
    createCanvas(640, 480);

    incf = TWO_PI/300;
    inc2 = TWO_PI/700;
    inc3 = TWO_PI/79;

    textFont("monospace")
    textSize(40)
    textStyle(BOLD)
}

function draw() {
    background(0)
    dname()
}  

function dname() {
    var i = 0;
    "DANIEL".split("").forEach(l => {
        letter(l, -i, xof, yof);
        i += 1;
    });

    i = 0;
    "MATHIAS".split("").forEach(l => {
        letter(l, -i, xofm, yofm);
        i += 1;
    });
}

function xofm(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)
    
    return width/2 - 200*s;
}

function yofm(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)

    var f4 = cos((f+50)*inc2)
    var f5 = cos((f+150)*inc3)


    return height/2 + 100*s*f5 - 100*fs*f5*f4 - 20*fs*f5;
}

function xof(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)

    var f4 = cos((f+50)*inc2)
    var f5 = cos((f+150)*inc3)

    return width/2 + 100*s*f4 - 100*f4*f5 + 40*fs*s;
}

function yof(f) {
    var fs = cos(f*incf)
    var s = -sin(f*incf)
    
    return height/2 + 200*s + 50*fs;
}


function letter(l, offset, xfunc, yfunc) {
    var fr = frameCount + offset*10
    
    var fs = cos(fr*incf)

    var s = -sin(fr*incf)
    
    var x = xfunc(fr)
    var y = yfunc(fr)
//    var x = width/2 + 200*s;
    // var y = height/2 + 200*s + 50*fs;

//    textSize(40+20*fs)
    strokeWeight(3)
    
    var col = abs(x/2-320)+160
    
    col = 255;
    stroke(0, 255, 0)
    fill(col, col, col)

    text(l, x, y)
//    rect(x, y, 3, 3)
}





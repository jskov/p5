function setup() {
    createCanvas(640, 480);
}

var x = 0;
var y = 0;
var w = 10;

function draw() {
    if (y >= height) {
        return;
    }    

    if (random() < 0.5) {
        line(x, y, x+w, y+w);
    } else {
        line(x+w, y, x, y+w);
    }

    x += w;
    if (x >= width) {
        x = 0;
        y += w;
    }
}  